--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.5
-- Dumped by pg_dump version 9.6.5

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: actors; Type: TABLE; Schema: public; Owner: lizhengxing
--

CREATE TABLE actors (
    actor_id integer NOT NULL,
    name character varying(50) NOT NULL
);


ALTER TABLE actors OWNER TO lizhengxing;

--
-- Name: actors_actor_id_seq; Type: SEQUENCE; Schema: public; Owner: lizhengxing
--

CREATE SEQUENCE actors_actor_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE actors_actor_id_seq OWNER TO lizhengxing;

--
-- Name: actors_actor_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lizhengxing
--

ALTER SEQUENCE actors_actor_id_seq OWNED BY actors.actor_id;


--
-- Name: comments; Type: TABLE; Schema: public; Owner: lizhengxing
--

CREATE TABLE comments (
    comment_id integer NOT NULL,
    user_id integer,
    comment character varying(100) NOT NULL,
    post_id integer
);


ALTER TABLE comments OWNER TO lizhengxing;

--
-- Name: comments_comment_id_seq; Type: SEQUENCE; Schema: public; Owner: lizhengxing
--

CREATE SEQUENCE comments_comment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE comments_comment_id_seq OWNER TO lizhengxing;

--
-- Name: comments_comment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lizhengxing
--

ALTER SEQUENCE comments_comment_id_seq OWNED BY comments.comment_id;


--
-- Name: movies; Type: TABLE; Schema: public; Owner: lizhengxing
--

CREATE TABLE movies (
    movie_id integer NOT NULL,
    title character varying(50) NOT NULL,
    writer character varying(50),
    year integer,
    franchise character varying(50),
    synopsis character varying(200)
);


ALTER TABLE movies OWNER TO lizhengxing;

--
-- Name: movies_actors; Type: TABLE; Schema: public; Owner: lizhengxing
--

CREATE TABLE movies_actors (
    movie_id integer,
    actor_id integer
);


ALTER TABLE movies_actors OWNER TO lizhengxing;

--
-- Name: movies_movie_id_seq; Type: SEQUENCE; Schema: public; Owner: lizhengxing
--

CREATE SEQUENCE movies_movie_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE movies_movie_id_seq OWNER TO lizhengxing;

--
-- Name: movies_movie_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lizhengxing
--

ALTER SEQUENCE movies_movie_id_seq OWNED BY movies.movie_id;


--
-- Name: posts; Type: TABLE; Schema: public; Owner: lizhengxing
--

CREATE TABLE posts (
    post_id integer NOT NULL,
    user_id integer,
    title character varying(100) NOT NULL,
    body character varying(100) NOT NULL
);


ALTER TABLE posts OWNER TO lizhengxing;

--
-- Name: posts_post_id_seq; Type: SEQUENCE; Schema: public; Owner: lizhengxing
--

CREATE SEQUENCE posts_post_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE posts_post_id_seq OWNER TO lizhengxing;

--
-- Name: posts_post_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lizhengxing
--

ALTER SEQUENCE posts_post_id_seq OWNED BY posts.post_id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: lizhengxing
--

CREATE TABLE users (
    user_id integer NOT NULL,
    username character varying(20) NOT NULL,
    first_name character varying(50) NOT NULL,
    last_name character varying(50) NOT NULL
);


ALTER TABLE users OWNER TO lizhengxing;

--
-- Name: users_user_id_seq; Type: SEQUENCE; Schema: public; Owner: lizhengxing
--

CREATE SEQUENCE users_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE users_user_id_seq OWNER TO lizhengxing;

--
-- Name: users_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lizhengxing
--

ALTER SEQUENCE users_user_id_seq OWNED BY users.user_id;


--
-- Name: actors actor_id; Type: DEFAULT; Schema: public; Owner: lizhengxing
--

ALTER TABLE ONLY actors ALTER COLUMN actor_id SET DEFAULT nextval('actors_actor_id_seq'::regclass);


--
-- Name: comments comment_id; Type: DEFAULT; Schema: public; Owner: lizhengxing
--

ALTER TABLE ONLY comments ALTER COLUMN comment_id SET DEFAULT nextval('comments_comment_id_seq'::regclass);


--
-- Name: movies movie_id; Type: DEFAULT; Schema: public; Owner: lizhengxing
--

ALTER TABLE ONLY movies ALTER COLUMN movie_id SET DEFAULT nextval('movies_movie_id_seq'::regclass);


--
-- Name: posts post_id; Type: DEFAULT; Schema: public; Owner: lizhengxing
--

ALTER TABLE ONLY posts ALTER COLUMN post_id SET DEFAULT nextval('posts_post_id_seq'::regclass);


--
-- Name: users user_id; Type: DEFAULT; Schema: public; Owner: lizhengxing
--

ALTER TABLE ONLY users ALTER COLUMN user_id SET DEFAULT nextval('users_user_id_seq'::regclass);


--
-- Data for Name: actors; Type: TABLE DATA; Schema: public; Owner: lizhengxing
--

COPY actors (actor_id, name) FROM stdin;
1	Brad Pitt
2	Edward Norton
3	John Travolta
4	Uma Thurman
5	Diane Kruger
6	Eli Roth
7	Samuel L. Jackson
\.


--
-- Name: actors_actor_id_seq; Type: SEQUENCE SET; Schema: public; Owner: lizhengxing
--

SELECT pg_catalog.setval('actors_actor_id_seq', 7, true);


--
-- Data for Name: comments; Type: TABLE DATA; Schema: public; Owner: lizhengxing
--

COPY comments (comment_id, user_id, comment, post_id) FROM stdin;
1	1	Hope you got a good deal!	4
2	1	What's mins is yours! 	5
3	1	Don't violate the licensing agreement!	6
4	2	It still isn't clean	1
5	2	Denied your PR cause I found a hack	3
\.


--
-- Name: comments_comment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: lizhengxing
--

SELECT pg_catalog.setval('comments_comment_id_seq', 5, true);


--
-- Data for Name: movies; Type: TABLE DATA; Schema: public; Owner: lizhengxing
--

COPY movies (movie_id, title, writer, year, franchise, synopsis) FROM stdin;
1	Fight Club	Chuck Palahniuk	1999		
2	Pulp Fiction	Quentin Tarantino	1994		
3	Inglorious Basterds	Quentin Tarantino	2009		
6	The Hobbit: The Battle of the Five Armies	J.R.R Tolkein	2012	The Hobbit	Bilbo and Company are forced to engage in a war against an array of combatants and keep the Lonely Mountain from falling into the hands of a rising darkness.
4	The Hobbit: An Unexpected Journey	J.R.R Tolkein	2012	The Hobbit	A reluctant hobbit, Bilbo Baggins, sets out to the Lonely Mountain with a spirited group of dwarves to reclaim their mountain home - and the gold within it - from the dragon Smaug.
5	The Hobbit: The Desolation of Smaug	J.R.R Tolkein	2013	The Hobbit	The dwarves, along with Bilbo Baggins and Gandalf the Grey, continue their quest to reclaim Erebor, their homeland, from Smaug. Bilbo Baggins is in possession of a mysterious and magical ring.
\.


--
-- Data for Name: movies_actors; Type: TABLE DATA; Schema: public; Owner: lizhengxing
--

COPY movies_actors (movie_id, actor_id) FROM stdin;
1	1
1	2
2	3
2	4
3	1
3	5
3	6
2	7
\.


--
-- Name: movies_movie_id_seq; Type: SEQUENCE SET; Schema: public; Owner: lizhengxing
--

SELECT pg_catalog.setval('movies_movie_id_seq', 8, true);


--
-- Data for Name: posts; Type: TABLE DATA; Schema: public; Owner: lizhengxing
--

COPY posts (post_id, user_id, title, body) FROM stdin;
1	1	Passes out at party	Wakes up early and clean house
2	1	Steals your identity	Raises your credit score
3	1	Reports a bug in your code	Sends you a Pull Request
4	2	Borrows something	Sells it
5	2	Borrows everything	The end
6	2	Folks your repo on github	Sets to private
\.


--
-- Name: posts_post_id_seq; Type: SEQUENCE SET; Schema: public; Owner: lizhengxing
--

SELECT pg_catalog.setval('posts_post_id_seq', 6, true);


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: lizhengxing
--

COPY users (user_id, username, first_name, last_name) FROM stdin;
1	GoodGuyGreg	Good Guy	Greg
2	ScumBagSteve	ScumBag	Steve
\.


--
-- Name: users_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: lizhengxing
--

SELECT pg_catalog.setval('users_user_id_seq', 2, true);


--
-- Name: actors actors_pkey; Type: CONSTRAINT; Schema: public; Owner: lizhengxing
--

ALTER TABLE ONLY actors
    ADD CONSTRAINT actors_pkey PRIMARY KEY (actor_id);


--
-- Name: comments comments_pkey; Type: CONSTRAINT; Schema: public; Owner: lizhengxing
--

ALTER TABLE ONLY comments
    ADD CONSTRAINT comments_pkey PRIMARY KEY (comment_id);


--
-- Name: movies movies_pkey; Type: CONSTRAINT; Schema: public; Owner: lizhengxing
--

ALTER TABLE ONLY movies
    ADD CONSTRAINT movies_pkey PRIMARY KEY (movie_id);


--
-- Name: posts posts_pkey; Type: CONSTRAINT; Schema: public; Owner: lizhengxing
--

ALTER TABLE ONLY posts
    ADD CONSTRAINT posts_pkey PRIMARY KEY (post_id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: lizhengxing
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (user_id);


--
-- Name: comments comments_post_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lizhengxing
--

ALTER TABLE ONLY comments
    ADD CONSTRAINT comments_post_id_fkey FOREIGN KEY (post_id) REFERENCES posts(post_id);


--
-- Name: comments comments_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lizhengxing
--

ALTER TABLE ONLY comments
    ADD CONSTRAINT comments_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(user_id);


--
-- Name: movies_actors movies_actors_actor_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lizhengxing
--

ALTER TABLE ONLY movies_actors
    ADD CONSTRAINT movies_actors_actor_id_fkey FOREIGN KEY (actor_id) REFERENCES actors(actor_id);


--
-- Name: movies_actors movies_actors_movie_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lizhengxing
--

ALTER TABLE ONLY movies_actors
    ADD CONSTRAINT movies_actors_movie_id_fkey FOREIGN KEY (movie_id) REFERENCES movies(movie_id);


--
-- Name: posts posts_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lizhengxing
--

ALTER TABLE ONLY posts
    ADD CONSTRAINT posts_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(user_id);


--
-- PostgreSQL database dump complete
--

