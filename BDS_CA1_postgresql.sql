CREATE DATABASE bigdata_ca1_zhengxing_li;

CREATE TABLE movies(
movie_id SERIAL PRIMARY KEY,
title varchar(50) NOT NULL,
writer varchar(50),
year int,
franchise varchar(50),
synopsis varchar(200));

INSERT INTO movies (title, writer, year, franchise, synopsis)
VALUES ('Fight Club', 'Chuck Palahniuk', 1999,'','');

INSERT INTO movies (title, writer, year, franchise, synopsis)
VALUES ('Pulp Fiction', 'Quentin Tarantino', 1994,'','');

INSERT INTO movies (title, writer, year, franchise, synopsis)
VALUES ('Inglorious Basterds', 'Quentin Tarantino', 2009,'','');

INSERT INTO movies (title, writer, year, franchise, synopsis)
VALUES ('The Hobbit: An Unexpected Journey', 'J.R.R Tolkein', 2012,'The Hobbit','');

INSERT INTO movies (title, writer, year, franchise, synopsis)
VALUES ('The Hobbit: The Desolation of Smaug', 'J.R.R Tolkein', 2013,'The Hobbit','');

INSERT INTO movies (title, writer, year, franchise, synopsis)
VALUES ('The Hobbit: The Battle of the Five Armies', 'J.R.R Tolkein', 2012,'The Hobbit','Bilbo and Company are forced to engage in a war against an array of combatants and keep the Lonely Mountain from falling into the hands of a rising darkness.');

INSERT INTO movies (title) VALUES ('Pee Wee Herman''s Big Adventure');

INSERT INTO movies (title) VALUES ('Avatar');

CREATE TABLE actors(
actor_id SERIAL PRIMARY KEY,
name varchar(50) NOT NULL);

INSERT INTO actors (name) VALUES ('Brad Pitt');
INSERT INTO actors (name) VALUES ('Edward Norton');
INSERT INTO actors (name) VALUES ('John Travolta');
INSERT INTO actors (name) VALUES ('Uma Thurman');
INSERT INTO actors (name) VALUES ('Diane Kruger');
INSERT INTO actors (name) VALUES ('Eli Roth');

CREATE TABLE movies_actors (
  movie_id int REFERENCES movies(movie_id),
  actor_id int REFERENCES actors(actor_id));

INSERT INTO movies_actors VALUES (1,1);
INSERT INTO movies_actors VALUES (1,2);
INSERT INTO movies_actors VALUES (2,3);
INSERT INTO movies_actors VALUES (2,4);
INSERT INTO movies_actors VALUES (3,1);
INSERT INTO movies_actors VALUES (3,5);
INSERT INTO movies_actors VALUES (3,6);

SELECT * FROM movies;

SELECT * FROM movies WHERE writer = 'Quentin Tarantino';

SELECT * FROM movies WHERE movie_id IN (SELECT movie_id FROM movies_actors WHERE actor_id = (SELECT actor_id FROM actors WHERE name = 'Brad Pitt'));

SELECT * FROM movies WHERE franchise = 'The Hobbit';

SELECT * FROM movies WHERE year > 1990 AND year < 2000;

SELECT * FROM movies WHERE year > 2010 OR year < 2000;

UPDATE movies SET synopsis ='A reluctant hobbit, Bilbo Baggins, sets out to the Lonely Mountain with a spirited group of dwarves to reclaim their mountain home - and the gold within it - from the dragon Smaug.'
WHERE title = 'The Hobbit: An Unexpected Journey';

SELECT * FROM movies WHERE title = 'The Hobbit: An Unexpected Journey';

UPDATE movies SET synopsis ='The dwarves, along with Bilbo Baggins and Gandalf the Grey, continue their quest to reclaim Erebor, their homeland, from Smaug. Bilbo Baggins is in possession of a mysterious and magical ring.'
WHERE title = 'The Hobbit: The Desolation of Smaug';

SELECT * FROM movies WHERE title = 'The Hobbit: The Desolation of Smaug';

INSERT INTO actors (name) VALUES ('Samuel L. Jackson');

INSERT into movies_actors VALUES ((SELECT movie_id FROM movies WHERE title = 'Pulp Fiction'),(SELECT actor_id FROM actors WHERE name = 'Samuel L. Jackson'));

SELECT * FROM movies WHERE synopsis LIKE '%Bilbo%';

SELECT * FROM movies WHERE synopsis LIKE '%Gandalf%';

SELECT * FROM movies WHERE synopsis LIKE '%Bilbo%' AND synopsis NOT LIKE '%Gandalf%';

SELECT * FROM movies WHERE synopsis LIKE '%dwarves%' OR synopsis LIKE '%hobbit%';

SELECT * FROM movies WHERE synopsis LIKE '%gold%' AND synopsis LIKE '%dragon%';

DELETE FROM movies WHERE title = 'Pee Wee Herman''s Big Adventure';
SELECT * FROM movies WHERE title = 'Pee Wee Herman''s Big Adventure';

DELETE FROM movies WHERE title = 'Avatar';
SELECT * FROM movies WHERE title = 'Avatar';

CREATE TABLE users(
user_id SERIAL PRIMARY KEY,
username varchar(20) NOT NULL,
first_name varchar(50) NOT NULL,
last_name varchar(50) NOT NULL);

INSERT INTO users (username, first_name, last_name) VALUES ('GoodGuyGreg', 'Good Guy', 'Greg');
INSERT INTO users (username, first_name, last_name) VALUES ('ScumBagSteve', 'ScumBag', 'Steve');


CREATE TABLE posts (
  post_id SERIAL PRIMARY KEY,
  user_id int REFERENCES users(user_id),
  title varchar(100) NOT NULL,
  body varchar(100) NOT NULL
);

INSERT INTO posts (user_id, title, body) VALUES ((SELECT user_id FROM users WHERE username = 'GoodGuyGreg'), 'Passes out at party', 'Wakes up early and clean house');

INSERT INTO posts (user_id, title, body) VALUES ((SELECT user_id FROM users WHERE username = 'GoodGuyGreg'), 'Steals your identity', 'Raises your credit score');

INSERT INTO posts (user_id, title, body) VALUES ((SELECT user_id FROM users WHERE username = 'GoodGuyGreg'), 'Reports a bug in your code', 'Sends you a Pull Request');

INSERT INTO posts (user_id, title, body) VALUES ((SELECT user_id FROM users WHERE username = 'ScumBagSteve'), 'Borrows something', 'Sells it');

INSERT INTO posts (user_id, title, body) VALUES ((SELECT user_id FROM users WHERE username = 'ScumBagSteve'), 'Borrows everything', 'The end');

INSERT INTO posts (user_id, title, body) VALUES ((SELECT user_id FROM users WHERE username = 'ScumBagSteve'), 'Folks your repo on github', 'Sets to private');

CREATE TABLE comments (
  comment_id SERIAL PRIMARY KEY,
  user_id int REFERENCES users(user_id),
  comment varchar(100) NOT NULL,
  post_id int REFERENCES posts(post_id)
);

INSERT INTO comments (user_id, comment, post_id)
VALUES ((SELECT user_id FROM users WHERE username = 'GoodGuyGreg'), 'Hope you got a good deal!',
(SELECT post_id FROM posts WHERE title = 'Borrows something'));

INSERT INTO comments (user_id, comment, post_id)
VALUES ((SELECT user_id FROM users WHERE username = 'GoodGuyGreg'), 'What''s mins is yours! ',
(SELECT post_id FROM posts WHERE title = 'Borrows everything'));

INSERT INTO comments (user_id, comment, post_id)
VALUES ((SELECT user_id FROM users WHERE username = 'GoodGuyGreg'), 'Don''t violate the licensing agreement!',
(SELECT post_id FROM posts WHERE title = 'Folks your repo on github'));

INSERT INTO comments (user_id, comment, post_id)
VALUES ((SELECT user_id FROM users WHERE username = 'ScumBagSteve'), 'It still isn''t clean',
(SELECT post_id FROM posts WHERE title = 'Passes out at party'));

INSERT INTO comments (user_id, comment, post_id)
VALUES ((SELECT user_id FROM users WHERE username = 'ScumBagSteve'), 'Denied your PR cause I found a hack',
(SELECT post_id FROM posts WHERE title = 'Reports a bug in your code'));

SELECT * FROM users;

SELECT * FROM posts;

SELECT posts.*,users.username FROM posts JOIN users ON users.user_id = posts.user_id WHERE users.username = 'GoodGuyGreg';

SELECT posts.*,users.username FROM posts JOIN users ON users.user_id = posts.user_id WHERE users.username = 'ScumBagSteve';

SELECT * FROM comments;

SELECT comments.*,users.username FROM comments JOIN users ON users.user_id = comments.user_id WHERE users.username = 'GoodGuyGreg';

SELECT comments.*,users.username FROM comments JOIN users ON users.user_id = comments.user_id WHERE users.username = 'ScumBagSteve';

SELECT comments.*, posts.title as post_title FROM comments JOIN posts ON posts.post_id = comments.post_id WHERE posts.title = 'Reports a bug in your code';
