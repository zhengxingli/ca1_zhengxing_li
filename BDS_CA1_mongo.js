//1. Create database
use BDS_CA1_Zhengxing_Li;

//2. Insert Document
db.movies.insert({
  title:"Fight Club",
  writer: "Chuck Palahniuk",
  year:1999,
  actors:["Brad Pitt","Edward Norton"]
})

db.movies.insert({
  title:"Pulp Fiction",
  writer: "Quentin Tarantino",
  year:1994,
  actors:["John Travolta","Uma Thurman"]
})

db.movies.insert({
  title:"Inglorious Basterds",
  writer: "Quentin Tarantino",
  year:2009,
  actors:["Brad Pitt","Diane Kruger","Eli Roth"]
})

db.movies.insert({
  title: "The Hobbit: An Unexpected Journey",
  writer: "J.R.R Tolkein",
  year:2012,
  franchise:"The Hobbit"
})

db.movies.insert({
  title: "The Hobbit: The Desolation of Smaug",
  writer: "J.R.R Tolkein",
  year:2013,
  franchise:"The Hobbit"
})


db.movies.insert({
  title: "The Hobbit: The Battle of the Five Armies",
  writer: "J.R.R Tolkein",
  year:2012,
  franchise:"The Hobbit",
  synopsis:"Bilbo and Company are forced to engage in a war against an array of combatants and keep the Lonely Mountain from falling into the hands of a rising darkness."
})

db.movies.insert({
  title:"Pee Wee Herman's Big Adventure"
})

db.movies.insert({
  title:"Avatar"
})

//3. Query/ Find Document

//1) get all documents
db.movies.find().pretty();

//2) get all documents with `writer` set to "Quentin Tarantino"
db.movies.find({
  writer:"Quentin Tarantino"
}).pretty();

//3) get all documents where `actors` include "Brad Pitt"
db.movies.find({
  actors:/Brad Pitt/
}).pretty();

//4) get all documents with `franchise` set to "The Hobbit"
db.movies.find({
  franchise:"The Hobbit"
}).pretty();

//5) get all movies released in the 90s
db.movies.find({
  $and:[
    {year:{$lt:2000}},
    {year:{$gt:1990}}
  ]
}).pretty();

//6) get all movies released before the year 2000 or after 2010
db.movies.find({
  $or:[
    {year:{$lt:2000}},
    {year:{$gt:2010}}
  ]
}).pretty();

//4. Update documents
//1) add a synopsis to "The Hobbit: An Unexpected Journey" : "A reluctant hobbit, Bilbo Baggins, sets out to the Lonely Mountain with a spirited group of dwarves to reclaim their mountain home - and the gold within it - from the dragon Smaug."
db.movies.update(
  {"title":"The Hobbit: An Unexpected Journey"},
  {$set:{"synopsis":"A reluctant hobbit, Bilbo Baggins, sets out to the Lonely Mountain with a spirited group of dwarves to reclaim their mountain home - and the gold within it - from the dragon Smaug"}}
);
db.movies.find({
  title:"The Hobbit: An Unexpected Journey"
}).pretty();

//2) add a synopsis to "The Hobbit: The Desolation of Smaug" : "The dwarves, along with Bilbo Baggins and Gandalf the Grey, continue their quest to reclaim Erebor, their homeland, from Smaug. Bilbo Baggins is in possession of a mysterious and magical ring."
db.movies.update(
  {"title":"The Hobbit: The Desolation of Smaug"},
  {$set:{"synopsis":"The dwarves, along with Bilbo Baggins and Gandalf the Grey, continue their quest to reclaim Erebor, their homeland, from Smaug. Bilbo Baggins is in possession of a mysterious and magical ring"}}
);
db.movies.find({
  title:"The Hobbit: The Desolation of Smaug"
}).pretty();

//3)add an actor named "Samuel L. Jackson" to the movie "Pulp Fiction"
db.movies.update(
  {"title":"Pulp Fiction"},
  {$push:{"actors":"Samuel L. Jackson"}}
);
db.movies.find({
  title:"Pulp Fiction"
}).pretty();

//5. Text Search
//1) find all movies that have a synopsis that contains the word "Bilbo"
db.movies.find({
  synopsis:/Bilbo/
}).pretty();

//2) find all movies that have a synopsis that contains the word "Gandalf"
db.movies.find({
  synopsis:/Gandalf/
}).pretty();

//3) find all movies that have a synopsis that contains the word "Bilbo" and not the word "Gandalf"
db.movies.find({
  $and:[
    {synopsis:/Bilbo/},
    {synopsis:{$not:/Gandalf/}}
  ]
}).pretty();

//4) find all movies that have a synopsis that contains the word "dwarves" or "hobbit"
db.movies.find({
  $or:[
    {synopsis:/dwarves/},
    {synopsis:/hobbit/}
  ]
}).pretty();

//5) find all movies that have a synopsis that contains the word "gold" and "dragon"
db.movies.find({
  $and:[
    {synopsis:/gold/},
    {synopsis:/dragon/}
  ]
}).pretty();

//6. Delete documents
//1) delete the movie "Pee Wee Herman's Big Adventure"
db.movies.remove({
  title:"Pee Wee Herman's Big Adventure"
});
//2) delete the movie "Avatar"
db.movies.remove({
  title:"Avatar"
});

//7. Relationships
//Users Collection
db.users.insert({
  username:"GoodGuyGreg",
  first_name:"Good Guy",
  last_name:"Greg"
});

db.users.insert({
  username:"SumBagSteve",
  full_name:{
    first:"ScumBag",
    last:"Steve"
  }
});

//Posts Collection
db.posts.insert({
  username:"GoodGuyGreg",
  title:"Passes out at party",
  body:"Wakes up early and cleans house"
});

db.posts.insert({
  username:"GoodGuyGreg",
  title:"Steals your identity",
  body:"Raises your credit score"
});

db.posts.insert({
  username:"GoodGuyGreg",
  title:"Reports a bug in your code",
  body:"Sends you a Pull Request"
});

db.posts.insert({
  username:"ScumBagSteve",
  title:"Borrows something",
  body:"Sells it"
});

db.posts.insert({
  username:"ScumBagSteve",
  title:"Borrows everything",
  body:"The end"
});

db.posts.insert({
  username:"ScumBagSteve",
  title:"Folks your repo on github",
  body:"Sets to private"
});

//Comments Collection
db.comments.insert({
  username:"GoodGuyGreg",
  comment:"Hope you got a good deal!",
  post:{$ref:"posts",$id:db.posts.findOne({title:"Borrows something"})._id}
});

db.comments.insert({
  username:"GoodGuyGreg",
  comment:"What's mine is yours!",
  post:{$ref:"posts",$id:db.posts.findOne({title:"Borrows everything"})._id}
});

db.comments.insert({
  username:"GoodGuyGreg",
  comment:"Don't violate the licensing agreement!",
  post:{$ref:"posts",$id:db.posts.findOne({title:"Folks your repo on github"})._id}
});

db.comments.insert({
  username:"ScumBagSteve",
  comment:"It still isn't clean!",
  post:{$ref:"posts",$id:db.posts.findOne({title:"Passes out at party"})._id}
});

db.comments.insert({
  username:"ScumBagSteve",
  comment:"Denied your PR cause I found a hack",
  post:{$ref:"posts",$id:db.posts.findOne({title:"Reports a bug in your code"})._id}
});

//8. Querying related collections
//1) find all users
db.users.find().pretty();

//2) find all posts
db.posts.find().pretty();

//3) find all posts that was authored by "GoodGuyGreg"
db.posts.find({
  username:"GoodGuyGreg"
}).pretty();

//4) find all posts that was authored by "ScumbagSteve"
db.posts.find({
  username:"ScumBagSteve"
}).pretty();

//5) find all comments
db.comments.find({ _id: ObjectId("59fb16a5d64ca394fcc99a50")}).pretty();

//6) find all comments that was authored by "GoodGuyGreg"
db.comments.find({
  username:"GoodGuyGreg"
}).pretty();

//7) find all comments that was authored by "ScumbagSteve"
db.comments.find({
  username:"ScumBagSteve"
}).pretty();

//8) find all comments belonging to the post "Reports a bug in your code"
var post_id = db.posts.findOne({title:"Reports a bug in your code"})._id;
db.comments.find({
    "post.$id":  post_id
}).pretty();
